import json, requests, ast

# ------------
# load_json
# ------------
def load_json(filename):
    """
    return a python dict jsn
    filename a json file
    """
    with open(filename, encoding='utf-8') as file:
        jsn = json.load(file)
        file.close()
    return jsn

data = load_json("data.json")
i = 0
for recipe in data['recipes']:
    if i > 148:
        break
    for ingredient in recipe['ingredients']:
        if i > 148:
            break
        if 'image' in ingredient:
            break
        else:
            ingData = requests.get("https://api.spoonacular.com/food/ingredients/"+str(ingredient['id'])+"/information?apiKey=6faf6e611ea64ead8b8a46034934dd14").json()
            i += 1
            print(str(i)+"th api call")
            if 'image' in ingData:
                ingredient['image'] = "https://spoonacular.com/cdn/ingredients_500x500/"+str(ingData['image'])
            
with open("data.json", 'w+') as f:
    json.dump(data, f, indent=4)


