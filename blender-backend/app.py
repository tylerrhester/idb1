'''
API
'''
import subprocess
import sys
from flask import Flask, json, render_template, jsonify
from flask_cors import CORS
from models import app, db, Recipe, Ingredient, Wine
from subprocess import Popen, PIPE, run

CORS(app)
# ------------
# index
# ------------
@app.route('/')
def index():
    '''
    Default routing
    '''
    return render_template('hello.html')

# ------------
# recipejson
# ------------
@app.route('/api/recipe/<id>', methods=['GET'])
def single_recipe(id):
    '''
    Single recipe
    '''
    rq = db.session.query(Recipe).get(id)
    return jsonify(rq.serialize())


@app.route('/api/recipes/', methods=['GET'])
def all_recipes():
    '''
    All recipes
    '''
    rq = db.session.query(Recipe).all()
    return jsonify(Recipes=[e.serialize() for e in rq])

# ------------
# ingjson
# ------------
@app.route('/api/ingredient/<id>', methods=['GET'])
def single_ingredient(id):
    '''
    Single Ingredients
    '''
    iq = db.session.query(Ingredient).get(id)
    return jsonify(iq.serialize())

@app.route('/api/ingredients/', methods=['GET'])
def all_ingredients():
    '''
    All Ingredients
    '''
    iq = db.session.query(Ingredient).all()
    return jsonify(Ingredients=[e.serialize() for e in iq])

# ------------
# winejson
# -----------
@app.route('/api/wine/<id>', methods=['GET'])
def single_wine(id):
    '''
    Single Wine
    '''
    wq = db.session.query(Wine).get(id)
    return jsonify(wq.serialize())

@app.route('/api/wines/', methods=['GET'])
def all_wines():
    '''
    All Wines
    '''
    wq = db.session.query(Wine).all()
    return jsonify(Wines=[e.serialize() for e in wq])

@app.route('/api/runtests', methods=['GET'])
def runTests():
    [output, stderr] = subprocess.Popen([sys.executable,"test.py"], stderr=PIPE).communicate()
    print(stderr) # "stderr" saves the result of the python unit tests
    return {"testresult" : str(stderr)}

if __name__ == "__main__":
    app.run(host='0.0.0.0', port='8080')
