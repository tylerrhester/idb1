import json
import requests
from models import app, db, Recipe, Ingredient, Wine

# ------------
# load_json
# ------------
def load_json(filename):
    """
    return a python dict jsn
    filename a json file
    """
    with open(filename, encoding='utf-8') as file:
        jsn = json.load(file)
        file.close()

    return jsn

# ------------
# create_books
# ------------
def create_database():
    """
    populate recipe table
    """
    data = load_json('data.json')

    for recipe in data['recipes']:
        newRecipe = Recipe(title = recipe['title'], id = recipe['id'], summary = recipe['summary'], instructions = recipe['instructions'], sourceURL = recipe['sourceUrl'], image = recipe['image'], pricePerServing = recipe['pricePerServing'], readyMin = recipe['readyInMinutes'], numServings = recipe['servings'])
        db.session.add(newRecipe)
        db.session.commit()

        for ing in recipe['ingredients']:
            ingExists = False
            for existingIng in db.session.query(Ingredient).all():
                if ing['id'] == existingIng.id:
                    ingExists = True
                    existingIng.ingRecipes.append(newRecipe)
                    db.session.commit()
                    break
            if not ingExists:
                if 'image' in ing:
                    newIng = Ingredient(title = ing['title'], id = ing['id'], calories = ing['calories'], cost = ing['cost'], substitutes = ing['subs'], image = ing['image'])
                else:
                    newIng = Ingredient(title = ing['title'], id = ing['id'], calories = ing['calories'], cost = ing['cost'], substitutes = ing['subs'], image = "404 NOT FOUND")
                db.session.add(newIng)
                db.session.commit()
                newRecipe.recipeIngredients.append(newIng)
                db.session.commit()
                

        if recipe['wines'] != "No Wines found":
            for wine in recipe['wines']:
                wineExists = False
                for existingWine in db.session.query(Wine).all():
                    if wine['id'] == existingWine.id:
                        wineExists = True
                        existingWine.wineRecipes.append(newRecipe)
                        db.session.commit()
                        break
                if not wineExists:
                    newWine = Wine(title = wine['title'], id = wine['id'], description = wine['desc'], sourceURL = wine['link'], sourceIMG = wine['imageUrl'], rating = wine['rating'], price = wine['price'], ratingCount = wine['ratingCount'])
                    db.session.add(newWine)
                    db.session.commit()
                    newRecipe.recipeWines.append(newWine)
                    db.session.commit()

        #establish wine-ingredient relationship
        for wine in db.session.query(Wine).all():
            for ing in db.session.query(Ingredient).all():
                ing.ingWines.append(wine)
                db.session.commit()


        
            
        
db.drop_all()
db.create_all()

create_database()
