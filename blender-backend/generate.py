import json, requests, ast

# ------------
# load_json
# ------------
def load_json(filename):
    """
    return a python dict jsn
    filename a json file
    """
    with open(filename, encoding='utf-8') as file:
        jsn = json.load(file)
        file.close()
    return jsn

data = load_json("data.json")
recipeIDs = [716431, 651409, 645265, 663696, 657556, 649361, 633492, 715407, 1098388, 637593, 648339, 656544, 157344, 635552, 715424, 640677, 662694, 635561, 652976, 634545, 641202, 665779, 632502, 635063, 637624, 650939, 640700, 651453, 641726, 715455, 715452, 798400, 641730, 665276, 644800, 649931, 654028, 665294, 632015, 642259, 715477, 634585, 659674, 644828, 975070, 641759, 636641, 652002, 647395, 639714, 715493, 660707, 715495, 632552, 635113, 640141, 637675, 633067, 644846, 642287, 633072, 648432, 803061, 649977, 646905, 715515, 646906, 644861, 632572, 645884, 644860, 637690, 662276, 782598, 647433, 636178, 715538, 640275, 663832, 653593, 715546, 632091, 633624, 642332, 782624, 730914, 639779, 637222, 715560, 655145, 660266, 715569, 632625, 632115, 1005367, 638779, 640827, 640318, 637761, 660290, 662850, 637767, 633160, 639303, 640844, 632140, 634191, 634703, 638808, 649049, 662363, 661340, 634873, 636766, 649567, 663392, 636768, 660322, 982365, 645988, 641893, 724324, 657761, 641896, 634217, 660842, 664429, 636271, 641908, 655733, 641911, 641912, 795514, 634237, 641410, 636292, 633221, 991625, 636300, 654735, 660368, 632208, 647577, 664473, 716198, 663462, 716202, 640941, 663985, 639411, 660404, 664501, 641461, 632244, 633786, 716218, 632250, 636864, 631748, 631751, 638409, 636363, 636365, 632790, 649183, 634854, 631783, 657384, 632299, 649195, 651757, 642540, 648176, 633330, 716276, 651765, 715769, 633338, 640508, 657917, 650239]
i = 8
idsList = (str(recipeIDs[0:i])[1:(len(str(recipeIDs[0:i])) - 1)]).replace(" ", "")

recipes = requests.get("https://api.spoonacular.com/recipes/informationBulk?apiKey=1988e57df6cf4a43aab2993252cc6a27&ids="+idsList).json()
for recipe in recipes:
    newRecipe = {}
    newRecipe['title'] = recipe['title']
    newRecipe['id'] = recipe['id']
    newRecipe['summary'] = recipe['summary']
    newRecipe['instructions'] = recipe['instructions']
    newRecipe['sourceUrl'] = recipe['sourceUrl']
    if 'image' not in recipe:
        newRecipe['image'] = '404 NOT FOUND'
    else:
        newRecipe['image'] = recipe['image']
    newRecipe['pricePerServing'] = recipe['pricePerServing']
    newRecipe['readyInMinutes'] = recipe['readyInMinutes']
    newRecipe['servings'] = recipe['servings']
    newRecipe['ingredients'] = []
    newRecipe['wines'] = []
    for ingredient in recipe['extendedIngredients']:
        if ingredient['id'] is not None:
            newIng = {}
            newIng['id'] = ingredient['id']
            ingData = requests.get("https://api.spoonacular.com/food/ingredients/"+str(newIng['id'])+"/information?amount=1&apiKey=1988e57df6cf4a43aab2993252cc6a27").json()
            if 'status' in ingData:
                if ingData['status'] == 'failure':
                    continue

            if 'name' in ingData:
                newIng['title'] = ingData['name']
            elif 'original' in ingData:
                newIng['title'] = ingData['original']
            else:
                newIng['title'] = "404 NOT FOUND"
            
            newIng['cost'] = 0
            if 'estimatedCost' in ingData:
                if 'value' in ingData['estimatedCost']:
                    newIng['cost'] = ingData['estimatedCost']['value']

            for nutrient in ingData['nutrition']['nutrients']:
                if nutrient['title'] == "Calories":
                    newIng['calories'] = nutrient['amount']
                    break
            ingSubs = requests.get("https://api.spoonacular.com/food/ingredients/"+str(newIng['id'])+"/substitutes?apiKey=1988e57df6cf4a43aab2993252cc6a27").json()
            if 'substitutes' in ingSubs:
                newIng['subs'] = ingSubs['substitutes']
            else:
                newIng['subs'] = 'No substitutes found'
            newRecipe['ingredients'].append(newIng)

    if recipe['winePairing']:
        if 'productMatches' in recipe['winePairing']:
            if recipe['winePairing']['productMatches']:
                for wine in recipe['winePairing']['productMatches']:
                    newWine = {}
                    newWine['id'] = wine['id']
                    newWine['title'] = wine['title']
                    newWine['desc'] = wine['description']
                    newWine['price'] = wine['price']
                    newWine['imageUrl'] = wine['imageUrl']
                    newWine['rating'] = wine['averageRating']
                    newWine['ratingCount'] = wine['ratingCount']
                    newWine['link'] = wine['link']
                    newRecipe['wines'].append(newWine)
            else:
                newRecipe['wines'] = "No Wines found"
        else:
            newRecipe['wines'] = "No Wines found"
    else:
        newRecipe['wines'] = "No Wines found"
        
    data['recipes'].append(newRecipe)

with open("data.json", 'w+') as f:
    json.dump(data, f, indent=4)




