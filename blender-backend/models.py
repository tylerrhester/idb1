from flask import Flask
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
import os

# initializing Flask app
app = Flask(__name__)

# Change this accordingly
USER ="postgres"
PASSWORD ="recipesarefun"
PUBLIC_IP_ADDRESS ="localhost:5432"
DBNAME ="blenderdb"

db_user = 'postgres'
db_password = 'recipesarefun'
db_name = 'blenderdb'
db_connection_name = 'friendly-autumn-317817:us-central1:blendrdb'

# Make these command line arguments that provide when you deploy the app
# or use other options like connecting directly from App Engine

# Configuration
# f'postgresql://{db_user}:{db_password}@/{db_name}?host=/cloudsql/{db_connection_name}'

app.config['SQLALCHEMY_DATABASE_URI'] = \
os.environ.get("DB_STRING",f'postgresql://{USER}:{PASSWORD}@{PUBLIC_IP_ADDRESS}/{DBNAME}')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True  # To suppress a warning message
db = SQLAlchemy(app)


recipes_to_ing = db.Table('recipes_to_ing',
   db.Column('recipe_id', db.Integer, db.ForeignKey('recipe.id')),
   db.Column('ingredient_id', db.Integer, db.ForeignKey('ingredient.id'))
   )
recipes_to_wine = db.Table('recipes_to_wine',
   db.Column('recipe_id', db.Integer, db.ForeignKey('recipe.id')),
   db.Column('wine_id', db.Integer, db.ForeignKey('wine.id'))
   )
ing_to_wine = db.Table('ing_to_wine',
   db.Column('ingredient_id', db.Integer, db.ForeignKey('ingredient.id')),
   db.Column('wine_id', db.Integer, db.ForeignKey('wine.id'))
   )
# ------------
# Recipe
# ------------
class Recipe(db.Model):

    __tablename__ = 'recipe'

    title = db.Column(db.String(250), nullable = False)
    id = db.Column(db.Integer, primary_key = True)
    summary = db.Column(db.Text)
    instructions = db.Column(db.Text)
    sourceURL = db.Column(db.String(250))
    image = db.Column(db.String(500))

    pricePerServing = db.Column(db.Float)
    readyMin = db.Column(db.Integer)
    numServings = db.Column(db.Integer)
    recipeIngredients = db.relationship('Ingredient', secondary = 'recipes_to_ing', back_populates='ingRecipes')
    recipeWines = db.relationship('Wine', secondary = 'recipes_to_wine', back_populates='wineRecipes')

    # ------------
    # serialize
    # ------------
    def serialize(self):
       """
       returns a dictionary
       """
       return {
          'id': self.id,
          'title': self.title,
          'summary': self.summary,
          'instructions': self.instructions,
          'sourceURL': self.sourceURL,
          'image': self.image,
          'pricePerServing': self.pricePerServing,
          'readyMin': self.readyMin,
          'numServings': self.numServings,
          'recipeIngredients': [ing.minSerialize() for ing in self.recipeIngredients],
          'recipeWines': [wine.minSerialize() for wine in self.recipeWines]
        }
    # ------------
    # minSerialize
    # ------------
    def minSerialize(self):
       """
       returns a dictionary
       """
       return {
          'id': self.id,
          'title': self.title,
          'summary': self.summary,
          'instructions': self.instructions,
          'sourceURL': self.sourceURL,
          'image': self.image,
          'pricePerServing': self.pricePerServing,
          'readyMin': self.readyMin,
          'numServings': self.numServings
        }

# ------------
# Ingredient
# ------------
class Ingredient(db.Model):

    __tablename__ = 'ingredient'

    title = db.Column(db.String(80), nullable = False)
    id = db.Column(db.Integer, primary_key = True)
    substitutes = db.Column(db.Text)
    calories = db.Column(db.Float)
    cost = db.Column(db.Float)
    image = db.Column(db.String(500))

    ingWines = db.relationship('Wine', secondary = 'ing_to_wine', back_populates='wineIngs')
    ingRecipes = db.relationship('Recipe', secondary = 'recipes_to_ing', back_populates='recipeIngredients')

    # ------------
    # serialize
    # ------------
    def serialize(self):
       """
       returns a dictionary
       """
       return {
          'id': self.id,
          'title': self.title,
          'substitutes': self.substitutes,
          'calories': self.calories,
          'cost': self.cost,
          'image': self.image,
          'ingWines': [wine.minSerialize() for wine in self.ingWines],
          'ingRecipes': [recipe.minSerialize() for recipe in self.ingRecipes]
        }
    # ------------
    # minSerialize
    # ------------
    def minSerialize(self):
       """
       returns a dictionary
       """
       return {
          'id': self.id,
          'title': self.title,
          'substitutes': self.substitutes,
          'calories': self.calories,
          'cost': self.cost,
          'image': self.image
        }

# ------------
# Wine
# ------------
class Wine(db.Model):

    __tablename__ = 'wine'

    title = db.Column(db.String(80), nullable = False)
    id = db.Column(db.Integer, primary_key = True)
    description = db.Column(db.Text)
    sourceURL = db.Column(db.String(250))
    sourceIMG = db.Column(db.String(250))
    rating = db.Column(db.Float)
    price = db.Column(db.String(80))
    ratingCount = db.Column(db.Integer)

    wineIngs = db.relationship('Ingredient', secondary = 'ing_to_wine', back_populates='ingWines')
    wineRecipes = db.relationship('Recipe', secondary = 'recipes_to_wine', back_populates='recipeWines')

    # ------------
    # serialize
    # ------------
    def serialize(self):
       """
       returns a dictionary
       """
       return {
          'id': self.id,
          'title': self.title,
          'description': self.description,
          'sourceURL': self.sourceURL,
          'sourceIMG': self.sourceIMG,
          'rating': self.rating,
          'price': self.price,
          'ratingCount': self.ratingCount,
          'wineIngs': [ing.minSerialize() for ing in self.wineIngs],
          'wineRecipes': [recipe.minSerialize() for recipe in self.wineRecipes]
        }
    # ------------
    # minSerialize
    # ------------
    def minSerialize(self):
       """
       returns a dictionary
       """
       return {
          'id': self.id,
          'title': self.title,
          'description': self.description,
          'sourceURL': self.sourceURL,
          'sourceIMG': self.sourceIMG,
          'rating': self.rating,
          'price': self.price,
          'ratingCount': self.ratingCount
        }



db.create_all()
