import os
import sys
import unittest
from models import db, Recipe, Ingredient, Wine

# -----------
# DBTestCases
# -----------
class DBTestCases(unittest.TestCase):

    def test_recipe_insert(self):
        s = Recipe(id='10', title = 'Test Recipe 1')
        db.session.add(s)
        db.session.commit()
        r = db.session.query(Recipe).filter_by(id = '10').one()
        self.assertEqual(str(r.title), 'Test Recipe 1')

    def test_ingredient_insert(self):
        s = Ingredient(id='20', title = 'Test Ingredient 1')
        db.session.add(s)
        db.session.commit()
        r = db.session.query(Ingredient).filter_by(id = '20').one()
        self.assertEqual(str(r.title), 'Test Ingredient 1')

    def test_wine_insert(self):
        s = Wine(id='30', title = 'Test Wine 1')
        db.session.add(s)
        db.session.commit()
        r = db.session.query(Wine).filter_by(id = '30').one()
        self.assertEqual(str(r.title), 'Test Wine 1')

    def test_recipe_ingredient_rel(self):
        r = Recipe(id='11', title = 'Test Recipe 2')
        s = Ingredient(id='21', title = 'Test Ingredient 2')
        db.session.add(s)
        db.session.add(r)
        db.session.commit()
        r.recipeIngredients.append(s)
        db.session.commit()
        self.assertEqual(str(s.ingRecipes[0].title), 'Test Recipe 2')

    def test_recipe_wine_rel(self):
        r = Recipe(id='12', title = 'Test Recipe 3')
        s = Wine(id='31', title = 'Test Wine 2')
        db.session.add(s)
        db.session.add(r)
        db.session.commit()
        r.recipeWines.append(s)
        db.session.commit()
        self.assertEqual(str(s.wineRecipes[0].title), 'Test Recipe 3')

    def test_ingredient_wine_rel(self):
        r = Ingredient(id='22', title = 'Test Ingredient 3')
        s = Wine(id='32', title = 'Test Wine 3')
        db.session.add(s)
        db.session.add(r)
        db.session.commit()
        r.ingWines.append(s)
        db.session.commit()
        self.assertEqual(str(s.wineIngs[0].title), 'Test Ingredient 3')

if __name__ == '__main__':
    # db.drop_all()
    db.create_all()
    unittest.main()
