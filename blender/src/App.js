import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import landing from "./components/pages/Landing.js"
import Search from './components/pages/Search.js';
import wines from "./components/pages/Wines.js"
import recipes from "./components/pages/Recipes.js"
import ingredients from "./components/pages/Ingredients.js"
import about from "./components/pages/About.js"
import React from 'react';
import RecipeDetails from "./components/pages/RecipeDetails"
import IngredientDetails from "./components/pages/IngredientDetails"
import WineDetails from "./components/pages/WineDetails"


function App() {

    return (
        
        <Router>
            <Switch>
                
                <Route exact path = "/" component = {landing} />
                <Route exact path = "/wines" component = {wines} />
                <Route exact path = "/recipes" component = {recipes} />
                <Route exact path = "/ingredients" component = {ingredients} />
                <Route exact path = "/about" component = {about} />
                <Route exact path="/search/s=:id" render={(props) => <Search searchTerm={props.match.params.id} />}/>

                <Route path = "/recipe/:id" component = {RecipeDetails} />
                <Route path = "/ingredient/:id" component = {IngredientDetails} />
                <Route path = "/wine/:id" component = {WineDetails} />
                
            </Switch>
        </Router>
        
    );
}

export default App;
