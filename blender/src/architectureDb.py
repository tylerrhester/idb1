import os
from flask import Flask
from flask_sqlalchemy import SQLAlchemy

# login config
USER = "postgres"
Password = "abc123"
PUBLIC_IP_ADDRESS = "localhost:5432"
DBNAME = "recipedb"

# Connect SQLAlchemy to PostgreSQL
app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = \
os.environ.get("DB_STRING", f'postgresql: //{USER} @ {PUBLIC_IP_ADDRESS}/{DBNAME}')
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = True 
db = SQLAlchemy(app)

# set up relationships
recipe_wine = db.Table("recipe",
    db.Column("recipe_id", db.Integer, db.ForeignKey("recipe.recipe_id")),
    db.Column("wine_id", db.Integer, db.ForeignKey("wine.wine_id"))
)

recipe_ingredient = db.Table("ingredient",
    db.Column("ingredient_id", db.Integer, db.ForeignKey("ingredient.ingredient_id")),
    db.Column("recipe_id", db.Integer, db.ForeignKey("recipe.recipe_id"))
)

# create classes
class recipe(db.Model):
    """SQLAlchemy class for recipes"""
    __tablename__ = "recipe"
    recipe_id = db.Column(db.Integer, primary_key=True)
    recipe_name = db.Column(db.String)
    recipe_ingredient = db.relationship("ingredient", secondary=recipe_ingredient, backref=db.backref("ingredient_recipe", lazy="dynamic"))
    recipe_wine = db.relationship("wine", secondary=recipe_wine, backref=db.backref("wine_recipe", lazy="dynamic"))
    recipe_image_url = db.Column(db.String)
    recipe_spoonacular_url = db.Column(db.String)
    recipe_spoonacular_id = db.Column(db.String)
    recipe_hasinfo = db.Column(db.Boolean)

    def __init__(self, name, id, image_url="", spoonacular_url="", spoonacular_id="", hasinfo=False):
        self.recipe_id = id
        self.recipe_name = name
        self.recipe_image_url = image_url
        self.recipe_spoonacular_url = spoonacular_url
        self.recipe_spoonacular_id = spoonacular_id
        self.recipe_hasinfo = hasinfo
    
    def __eq__(self, other):
        return self.recipe_name == other.recipe_name

class ingredient(db.Model):
    """SQLAlchemy class for ingredients"""
    __tablename__ = "recipe"
    ingredient_id = db.Column(db.Integer, primary_key=True)
    ingredient_name = db.Column(db.String)
    ingredient_wine = db.relationship("wine", backref=db.backref("wine_ingredient"))
    ingredient_image_url = db.Column(db.String)
    ingredient_spoonacular_url = db.Column(db.String)
    ingredient_spoonacular_id = db.Column(db.String)

    def __init__(self, name, id, image_url="", spoonacular_url="", spoonacular_id=""):
        self.ingredient_id = id
        self.ingredient_name = name
        self.ingredient_image_url = image_url
        self.ingredient_spoonacular_url = spoonacular_url
        self.ingredient_spoonacular_id = spoonacular_id

class wine(db.Model):
    """SQLAlchemy class for wines"""
    __tablename__ = "wine"
    wine_id = db.Column(db.Integer, primary_key=True)
    wine_name = db.Column(db.String)
    wine_rating = db.Column(db.Integer)
    wine_price = db.Column(db.Integer)
    wine_ingredient_id = db.Column(db.Integer, db.ForeignKey("ingredient.ingredient_id"))
    wine_description = db.Column(db.String)
    wine_image_url = db.Column(db.String)
    wine_spoonacular_url = db.Column(db.String)
    wine_spoonacular_id = db.Column(db.String)


db.create_all()
db.session.commit()