import React, {useState} from 'react';
import '../App.css';
import { Button } from './CustomButton';
import './HomeLayout.css';
import video from '../assets/video.mp4';
import Typical from 'react-typical'


function HomeLayout() {

  var [searchTerm, setSearchTerm] = useState("");
  const BarStyling = {width:"23rem",background:"#F2F1F9", border:"none", padding:"0.7rem"};

    return (
      <div className='hero-container'>
        <video src={video} autoPlay loop muted />

        <h1>Blender</h1>  
        <div style={{color: "white"}}>
        <h1 className = 'heading-typing'> 
            <Typical
            loop = {Infinity}
            wrapper = "b"
            steps = {[
              ' Eat. Live. Drink!',
              2000,
              ' Delicous Healthy Meals.',
              2000,
              ' No Ads.',
              2000
            ]}
            />
        </h1>
        </div>
        
        <p>Find out what you can cook.</p>
        
        <form action={"/search/s=" + searchTerm} method="get">
            <input 
                type="text"
                id="header-search"
                name="s" 
                style={BarStyling}
                value={searchTerm}
                placeholder={"search recipes, ingredients, or wines"}
                onChange={(e) => setSearchTerm(e.target.value)}
            />
            {' '}<Button type="submit">Search</Button>{' '}
        </form>

      </div>
      
    );
  }
  
  export default HomeLayout