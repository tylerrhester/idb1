import React, {useEffect, useState} from 'react';
import { Link } from 'react-router-dom';
import './Navbar.css';
import logo2 from '../assets/logo2.png'



function Navbar(){
    const [click, setClick] = useState(false);
    const [button1, setButton] = useState(true);
    const [navbar, setNavBar] = useState(false);
    const handleClick = () => setClick(!click);

    const closeMobileMenu = () => setClick(false);
    const showButton = () => {
        if(window.innerWidth <= 960){
            setButton(false);
        }else{
            setButton(true);
        }
    }
    useEffect(() => {
        showButton()
    }, []);

    window.addEventListener('resize', showButton);


    const changeBackground = () => {
        if(window.scrollY >= 0){
            setNavBar(true);
        }else{
            setNavBar(false);
        }
    }
    window.addEventListener('scroll', changeBackground);


    return(
        <>
            <nav className = 'navbar active'>
                <div className = "navbar-container">
                    <div className = 'logo-container'>
                        <Link to = "/" className = "navbar-logo" onClick={closeMobileMenu}>
                            <img className = 'cmp-logo'
                                src={logo2}
                                alt="logo"/>
                        </Link>
                    </div>
                    <div className='menu-icon' onClick={() => {setNavBar(); handleClick(); }}>
                        <i className = {click ? 'fas fa-times' : 'fas fa-bars'}/>
                    </div>
                    <ul className={click ? 'nav-menu active': 'nav-menu'}>
                        <li className='nav-item'>
                            <Link to='/' className='nav-links' onClick={closeMobileMenu}> Home </Link>
                        </li>
                        <li className='nav-item'>
                            <Link to='/about' className='nav-links' onClick={closeMobileMenu}> About Us </Link>
                        </li>
                        <li className='nav-item'>
                            <Link to='/recipes' className='nav-links' onClick={closeMobileMenu}> Recipes </Link>
                        </li>
                        <li className='nav-item'>
                            <Link to='/ingredients' className='nav-links' onClick={closeMobileMenu}> Ingredients </Link>
                        </li>
                        <li className='nav-item'>
                            <Link to='/wines' className='nav-links' onClick={closeMobileMenu}> Wines </Link>
                        </li>
                    </ul>
                </div>
            </nav>
        </>
    )
}

export default Navbar
