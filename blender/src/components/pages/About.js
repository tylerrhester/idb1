import Card from 'react-bootstrap/Card';
import ListGroupItem from 'react-bootstrap/ListGroupItem'
import ListGroup from 'react-bootstrap/ListGroup'
import "../../App.css";
import faizphoto from "../../assets/photos/faiz.jpg"
import tylerphoto from "../../assets/photos/tylerheadshot.jpg"
import manavphoto from "../../assets/photos/manav-headshot.jpg"
import chrissyphoto from "../../assets/photos/profheadshot2.jpg"
import yingleiphoto from "../../assets/photos/yinglei.jpg"
import Navbar2 from '../Navbar2.js'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Container from 'react-bootstrap/Container'
import React, { useState, useEffect } from 'react';
import './About.css'
import Button from 'react-bootstrap/Button';




function About() {

    const[showMessage, setShowMessage] = useState();

    useEffect(() => {fetch("http://api-dot-friendly-autumn-317817.uc.r.appspot.com/api/runtests")
        .then(response => response.json())
        .then(json => setShowMessage(JSON.stringify(json)))
    }, [])

    const [divColor, setBagColor] = useState(false);
    const changeBackground = () => {
        if(window.scrollY >= 130){
            setBagColor(true);
        }else{
            setBagColor(false);
        }
    }
    window.addEventListener('scroll', changeBackground);

    return (
    <div className= {divColor ? 'primary-container-active' : 'primary-container'}>
        <Navbar2 />
        <h1 className='p-2 text-center bg-light'>About us</h1>
        <h4 className='text-center'>What is Blender?</h4>
        <div class="p-2 container text-center">Blender is a website that allows you to check out common recipes,
        ingredients, and wine pairings. It's all in one database, so you don't have to do any work!</div>

         <Container fluid className= {divColor ? 'primary-container-active' : 'primary-container'}>
         <Row className = {divColor ? 'primary-container-active' : 'primary-container'}>
         <Col classname = {divColor ? 'primary-container-active' : 'primary-container'}>
            <Card className = {divColor ? 'primary-container-active' : 'primary-container'}>
            <Card.Img variant="top" src = {faizphoto} />
            <Card.Body className = {divColor ? 'primary-container-active' : 'primary-container'}>
                <Card.Title className = {divColor ? 'primary-container-active' : 'primary-container'}>Faiz Prasla</Card.Title>
            </Card.Body>
            <ListGroup className = {divColor ? 'primary-container-active' : 'primary-container'}>
                <ListGroupItem className = {divColor ? 'primary-container-active' : 'primary-container'}> Frontend and Deployment Engineer </ListGroupItem>
                <ListGroupItem className = {divColor ? 'primary-container-active' : 'primary-container'}> I am a senior at University of Texas at Austin studying Computer Science
                I have some background in front end reactJS. However, my expertise lies in traditianal netowkring
                and penetration testing. My hobbies are to play soccer, skydive, and engage in social gatherings. In my free time
                I have been trying to get my pliots VFR and skydiving lisence.
                </ListGroupItem>
                <ListGroupItem className = {divColor ? 'primary-container-active' : 'primary-container'}> Commits: 19 </ListGroupItem>
                <ListGroupItem className = {divColor ? 'primary-container-active' : 'primary-container'}> Issues: 0 </ListGroupItem>
                <ListGroupItem className = {divColor ? 'primary-container-active' : 'primary-container'}> Unit tests: 0 </ListGroupItem>
            </ListGroup>
            </Card>
            </Col>

            <Col className = {divColor ? 'primary-container-active' : 'primary-container'}>
            <Card className = {divColor ? 'primary-container-active' : 'primary-container'}>
            <Card.Img variant="top" src = {tylerphoto} />
            <Card.Body>
                <Card.Title className = {divColor ? 'primary-container-active' : 'primary-container'}>Tyler Hester</Card.Title>
            </Card.Body>
            <ListGroup className = {divColor ? 'primary-container-active' : 'primary-container'}>
                <ListGroupItem className = {divColor ? 'primary-container-active' : 'primary-container'}> Backend and API Engineer </ListGroupItem>
                <ListGroupItem className = {divColor ? 'primary-container-active' : 'primary-container'}> I am currently a Senior at the University of Texas at
                    Austin in my final semester to complete my degree. I have a miriad
                    of work experience in Security, QA UI Automation, and Distributed Ledger
                    Technologies. I've developed websites for proof-of-concepts and love to
                    challenge myself with programming problems. Cat lover, and avid gamer
                    who loves to go hiking from time to time </ListGroupItem>
                <ListGroupItem className = {divColor ? 'primary-container-active' : 'primary-container'}> Commits: 17</ListGroupItem>
                <ListGroupItem className = {divColor ? 'primary-container-active' : 'primary-container'}> Issues: 11 </ListGroupItem>
                <ListGroupItem className = {divColor ? 'primary-container-active' : 'primary-container'}> Unit tests: 0</ListGroupItem>
            </ListGroup>
            </Card>
            </Col>

            <Col className = {divColor ? 'primary-container-active' : 'primary-container'}>
            <Card className = {divColor ? 'primary-container-active' : 'primary-container'}>
            <Card.Img variant="top" src={manavphoto} />
            <Card.Body className = {divColor ? 'primary-container-active' : 'primary-container'}>
                <Card.Title className = {divColor ? 'primary-container-active' : 'primary-container'}>Manav Sood</Card.Title>
            </Card.Body>
            <ListGroup className = {divColor ? 'primary-container-active' : 'primary-container'}>
                <ListGroupItem className = {divColor ? 'primary-container-active' : 'primary-container'}> Database and Deployment Engineer </ListGroupItem>
                <ListGroupItem className = {divColor ? 'primary-container-active' : 'primary-container'}> I am a senior majoring in Computer Science at
                    the University of Texas, and current Information Technology Event
                    Intern at Austin FC. My specializations lie in Cybersecurity, Networking,
                    and DevSecOps. I love computers, cars, and coffee, and my free time
                    I can usually be found at my PC gaming or watching Netflix. </ListGroupItem>

                <ListGroupItem className = {divColor ? 'primary-container-active' : 'primary-container'}> Commits: 32</ListGroupItem>
                <ListGroupItem className = {divColor ? 'primary-container-active' : 'primary-container'}> Issues: 0 </ListGroupItem>
                <ListGroupItem className = {divColor ? 'primary-container-active' : 'primary-container'}> Unit tests: 0 </ListGroupItem>
            </ListGroup>
            </Card>
            </Col>

            <Col className = {divColor ? 'primary-container-active' : 'primary-container'}>
            <Card className = {divColor ? 'primary-container-active' : 'primary-container'}>
            <Card.Img variant="top" src={chrissyphoto} />
            <Card.Body className = {divColor ? 'primary-container-active' : 'primary-container'}>
                <Card.Title className = {divColor ? 'primary-container-active' : 'primary-container'}>Chrissy Milich</Card.Title>
            </Card.Body>
            <ListGroup className = {divColor ? 'primary-container-active' : 'primary-container'}>
                <ListGroupItem className = {divColor ? 'primary-container-active' : 'primary-container'}> Frontend Engineer </ListGroupItem>
                <ListGroupItem className = {divColor ? 'primary-container-active' : 'primary-container'}> I currently pursuing a degree in Computer Science with
                    a minor in English from the University of Texas at Austin. My specializations
                    lie in Data Sciences and algorithms. I love music, movies and my dog, Doc! </ListGroupItem>
                <ListGroupItem className = {divColor ? 'primary-container-active' : 'primary-container'}> Commits: 17 </ListGroupItem>
                <ListGroupItem className = {divColor ? 'primary-container-active' : 'primary-container'}> Issues: 4 </ListGroupItem>
                <ListGroupItem className = {divColor ? 'primary-container-active' : 'primary-container'}> Unit tests: 0 </ListGroupItem>
            </ListGroup>
            </Card>
            </Col>

            <Col className = {divColor ? 'primary-container-active' : 'primary-container'}>
            <Card className = {divColor ? 'primary-container-active' : 'primary-container'}>
            <Card.Img variant="top" src = {yingleiphoto} />
            <Card.Body className = {divColor ? 'primary-container-active' : 'primary-container'}>
                <Card.Title className = {divColor ? 'primary-container-active' : 'primary-container'}>Yinglei Fang</Card.Title>
            </Card.Body>
            <ListGroup className = {divColor ? 'primary-container-active' : 'primary-container'}>
                <ListGroupItem className = {divColor ? 'primary-container-active' : 'primary-container'}> Backend Engineer </ListGroupItem>
                <ListGroupItem className = {divColor ? 'primary-container-active' : 'primary-container'}> I'm currently studying CS at the University of Texas at Austin.
                    I have a very wide range of interest from reading to music to gaming and a lot more. I
                    just started working at a healthcare AI company and I
                    ereally hope I can become a better programmer. </ListGroupItem>
                <ListGroupItem className = {divColor ? 'primary-container-active' : 'primary-container'}> Commits: 12 </ListGroupItem>
                <ListGroupItem className = {divColor ? 'primary-container-active' : 'primary-container'}> Issues: 4 </ListGroupItem>
                <ListGroupItem className = {divColor ? 'primary-container-active' : 'primary-container'}> Unit tests: 0 </ListGroupItem>
            </ListGroup>
            </Card>
            </Col>
         </Row>
        </Container>

        <br></br>
        <ListGroup className="list-group-flush" style = {{alignItems:'center'}}>
            <Button> CLICK HERE TO RUN UNIT TESTS </Button>
            <span>{showMessage}</span>
            <ListGroupItem className = {divColor ? 'primary-container-active' : 'primary-container'}> Total Commits: 112 </ListGroupItem>
            <ListGroupItem className = {divColor ? 'primary-container-active' : 'primary-container'}> Total Issues: 58 </ListGroupItem>
            <ListGroupItem className = {divColor ? 'primary-container-active' : 'primary-container'}> Total Unit tests: 6 </ListGroupItem>
            <ListGroupItem className = {divColor ? 'primary-container-active' : 'primary-container'}> <a href = "Postman API">  </a> </ListGroupItem>
            <ListGroupItem className = {divColor ? 'primary-container-active' : 'primary-container'}> <a href = "https://gitlab.com/tylerrhester/idb1/-/issues" > Issue Tracker </a> </ListGroupItem>
            <ListGroupItem className = {divColor ? 'primary-container-active' : 'primary-container'}> <a href = "https://gitlab.com/tylerrhester/idb1">  GitLab Repo</a> </ListGroupItem>
            <ListGroupItem className = {divColor ? 'primary-container-active' : 'primary-container'}> <a href = "https://documenter.getpostman.com/view/121179/TzedfPei">  Postman API</a> </ListGroupItem>
            <ListGroupItem className = {divColor ? 'primary-container-active' : 'primary-container'}> <a href = "https://gitlab.com/tylerrhester/idb1/-/wikis/Title"> GitLab Wiki  </a></ListGroupItem>
<<<<<<< HEAD
            <ListGroupItem className = {divColor ? 'primary-container-active' : 'primary-container'}> <a href = "https://spoonacular.com/food-api"> Data source: Spoonacular </a> </ListGroupItem>            <ListGroupItem className = {divColor ? 'primary-container-active' : 'primary-container'}> Scraping method: Spoonacular provided several databases for us, each scraped using Postman and placed into a JSON file.</ListGroupItem>
=======
            <ListGroupItem className = {divColor ? 'primary-container-active' : 'primary-container'}> <a href = "https://spoonacular.com/food-api"> Data source: Spoonacular </a> </ListGroupItem>
            <ListGroupItem className = {divColor ? 'primary-container-active' : 'primary-container'}> <a href = "https://speakerdeck.com/idb8pres/idb8-phase-3-presentation"> Presentation</a> </ListGroupItem>
            <ListGroupItem className = {divColor ? 'primary-container-active' : 'primary-container'}> Scraping method: Spoonacular provided several databases for us, each scraped using Postman and placed into a JSON file.</ListGroupItem>
>>>>>>> refactor
            <ListGroupItem className = {divColor ? 'primary-container-active' : 'primary-container'}> Tools: React, Bootstrap, Postman, GCP, Python, Flask, JavaScript, CSS, NameCheap, GitLab, CORS, PostgreSQL, SQLAlchemy, Axios </ListGroupItem>
        </ListGroup>

    </div>



    )
}
export default About
