import React, {useState} from 'react';
import { Card, ListGroup } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import './Recipes.css'
import Typography from '@material-ui/core/Typography'
import CardContent from '@material-ui/core/CardContent'
import { CardMedia } from '@material-ui/core'
import Cards from '@material-ui/core/Card'
import Highlighter from 'react-highlight-words'
import { makeStyles } from '@material-ui/core/styles'





function returnString(recipeData) {
    if (recipeData === undefined) {
        return 'N/A'
    } else {
        return recipeData.toString()
    }
}

const useStyles = makeStyles({
    root: {
        minWidth: 275,
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        fontSize: 14,
    },
    pos: {
        marginBottom: 12,
    },
    media: {
        height: 170,
    },
})

const Ingredient = ({ ingredientData, searchQuery }) => {

    const [divColor, setBagColor] = useState(false);
    const changeBackground = () => {
        if(window.scrollY >= 135){
            setBagColor(true);
        }else{
            setBagColor(false);
        }
    }
    window.addEventListener('scroll', changeBackground);

    const classes = useStyles()

    // var termArray = searchQuery.split(" ")


    return (
        <a href={'/ingredient/' + ingredientData.id}>
        <Cards className={classes.root} variant="outlined">
            <CardContent>
                <CardMedia
                    className={classes.media}
                    image={ingredientData.image}
                    title="image"
                />
                <Typography
                    className={classes.pos}
                    variant="h6"
                    component="h2"
                    align="center"
                >
                    <Highlighter
                        searchWords={[searchQuery]}
                        textToHighlight={ingredientData.title}
                    />
                </Typography>
                <Typography className={classes.pos}>
                    Calories: {ingredientData.calories}
                    <Highlighter
                        searchWords={[searchQuery]}
                        textToHighlight={ingredientData.pricePerServing}/>
                </Typography>
                <Typography className={classes.pos}>
                    Cost: ${ingredientData.cost}
                    <Highlighter
                        searchWords={[searchQuery]}
                        textToHighlight={ingredientData.cost}/>
                </Typography>
            </CardContent>
        </Cards>
        </a>

        // <Card style={{ width: '18rem' }}>
        //     <Card.Header className= {divColor ? 'primary-container-active' : 'primary-container'}>
        //         <Link className= {divColor ? 'primary-container-active1' : 'primary-container1'} to={{pathname:`/ingredient/${ingredientData.id}`, query: `${ingredientData.id}`}}>
        //             {ingredientData.title}
        //         </Link>
        //     </Card.Header>
        //     <Card.Img src={ingredientData.image} variant="top" />
        //     <Card.Body>

        //     </Card.Body>
        //     <ListGroup className= {divColor ? 'primary-container-active' : 'primary-container'}>
        //         <ListGroup.Item className= {divColor ? 'primary-container-active' : 'primary-container'}>Calories: {ingredientData.calories}</ListGroup.Item>
        //         <ListGroup.Item className= {divColor ? 'primary-container-active' : 'primary-container'}>Cost: ${ingredientData.cost}</ListGroup.Item>
        //     </ListGroup>
        // </Card>

    );
};

export default Ingredient;
