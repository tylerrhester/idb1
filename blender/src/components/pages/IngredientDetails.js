import React, { useEffect, useState } from 'react';

import Navbar2 from '../Navbar2.js'
import Ingredient from './IngredientCard.js'
import Recipe from './RecipeCard.js'
import Wine from './WineCard.js'
import CustomPagination from './Pagination.js';

import { Container, Row, Col } from 'react-bootstrap';
import { useParams } from 'react-router-dom';

import axios from 'axios';

const IngredientDetails = () => {

    const { id } = useParams();

    const [data, setData] = useState([]);
    const [wines, setWines] = useState([]);
    const [recipes, setRecipes] = useState([]);

    const [currWinePage, setCurrentWinePage] = useState(1);
    const [winesPerPage] = useState(3);
    const lastWine = currWinePage * winesPerPage;
    const firstWine = lastWine - winesPerPage;
    const currWines = wines.slice(firstWine, lastWine);
    const paginateWine = (pageNumber) => setCurrentWinePage(pageNumber);

    const [currRecipePage, setCurrentRecipePage] = useState(1);
    const [recipesPerPage] = useState(3);
    const lastRecipe = currRecipePage * recipesPerPage;
    const firstRecipe = lastRecipe - recipesPerPage;
    const currRecipes = recipes.slice(firstRecipe, lastRecipe);
    const paginateRecipe = (pageNumber) => setCurrentRecipePage(pageNumber);


    useEffect(() => {
        const getRecipe = async () => {
            const singleIngredient = await axios.get("http://api-dot-friendly-autumn-317817.uc.r.appspot.com/api/ingredient/"+id);
            setData(singleIngredient.data);
            setWines(singleIngredient.data.ingWines);
            setRecipes(singleIngredient.data.ingRecipes);
        };

        getRecipe();
    }, [id]);

    return (
        <div>
            <Navbar2 />
            <Container fluid="md mt-5">
                <Row className="mb-5">
                    <Col className="d-flex justify-content-center">
                        <Ingredient ingredientData={data}/>
                    </Col>
                </Row>

                <h3 className="text-center">Common Recipes:</h3>
                <Row className="justify-content-md-center mb-3">
                    <CustomPagination
                        itemsPerPage={recipesPerPage}
                        totalItems={recipes.length}
                        paginate={paginateRecipe}
                        className="justify-content-center"
                    />
                </Row>
                <Row className="justify-content-md-center">
                    {currRecipes.map(recipe => (
                        <Col>
                            <Recipe recipeData={recipe} />
                        </Col>
                    ))}
                </Row>

                <h3 className="text-center mt-3">Wine Pairings:</h3>
                <Row className="justify-content-md-center mb-3">
                    <CustomPagination
                        itemsPerPage={winesPerPage}
                        totalItems={wines.length}
                        paginate={paginateWine}
                        className="justify-content-center"
                    />
                </Row>
                <Row className="justify-content-md-center">
                    {currWines.map(wine => (
                        <Col>
                            <Wine wineData={wine} />
                        </Col>
                    ))}
                </Row>
            </Container>
        </div>
    );
};

export default IngredientDetails;
