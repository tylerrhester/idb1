import React from 'react'
import IngredientCard from './IngredientCard'
import { Grid } from '@material-ui/core'


function ingredientGrid({ ingredientData, searchQuery }) {
    return (
        <Grid container spacing={3} justify="center">
            {ingredientData.map((col) => {
                return (
                    <Grid item key={col.id} xs={4}>
                        <IngredientCard ingredientData={col} searchQuery={searchQuery} />
                    </Grid>
                )
            })}
        </Grid>
    )
}
export default ingredientGrid
