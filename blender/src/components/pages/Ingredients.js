import React, {useEffect, useState} from 'react';
import { Row, Col, Container } from 'react-bootstrap';
import axios from 'axios';

import Loading from './Loading.js';
import Ingredient from './IngredientCard.js';
import CustomPagination from './Pagination.js';
import Navbar2 from '../Navbar2.js'
import "../../App.css";
import './Recipes.css'
import Select from 'react-select'
import SearchBar from '../SearchBar.js'
import IngredientGrid from './IngredientGrid'
import { Pagination } from '@material-ui/lab'




const CardsPerPage = 9

function Ingredients() {

    const [divColor, setBagColor] = useState(false);
    const changeBackground = () => {
        if(window.scrollY >= 135){
            setBagColor(true);
        }else{
            setBagColor(false);
        }
    }
    window.addEventListener('scroll', changeBackground);




    var [ingredientData, setIngredientData] = useState([])
    var [currentData, setCurrentData] = useState([])
    var [activeFilter, setActiveFilter] = useState(0);
    var [activeSort, setActiveSort] = useState(0);
    var [searchData, setSearchData] = useState([])
    var [searchQuery, setSeachQuery] = useState('')

    const [data, setData] = useState([]);
    const [loading, setLoading] = useState(false);
    const [currPage, setCurrentPage] = useState(1);
    const [ingredientsPerPage] = useState(9);

    const lastIng = currPage * ingredientsPerPage;
    const firstIng = lastIng - ingredientsPerPage;
    const currIngredients = data.slice(firstIng, lastIng);
    const paginate = (pageNumber) => setCurrentPage(pageNumber);

    useEffect(() => {


        const getIngredients = async () => {
            setLoading(true);
            const allIngredients = await axios.get("http://api-dot-friendly-autumn-317817.uc.r.appspot.com/api/ingredients");
            setData(allIngredients.data.Ingredients);

            setIngredientData(allIngredients.data.Ingredients);
            setSearchData(allIngredients.data.Ingredients);
            setCurrentData(allIngredients.data.Ingredients.slice(0, CardsPerPage));

            setLoading(false);
        };

        getIngredients();
    }, []);

    if (loading) {
        return (
            <Loading />
        );
    }

    const filters = [
        {
            name: 'None',
            func: (a) => {
                return true
            },
        },
        {
            name: 'Calories < 10',
            func: (a) => {
                return a.calories < 10
            },
        },
        {
            name: ' 10 <= Calories <= 20',
            func: (a) => {
                return a.calories >= 10 && a.colories <= 20
            },
        },
        {
            name: 'Calories > 100',
            func: (a) => {
                return a.calories > 100
            },
        },
        {
            name: ' 0 <= cost <= 10',
            func: (a) => {
                return a.cost >= 0 && a.cost <= 10
            },
        },
        {
            name: ' 10 <= cost <= 20',
            func: (a) => {
                return a.cost >= 10 && a.cost <= 20
            },
        },
        {
            name: 'cost > 20',
            func: (a) => {
                return a.cost > 20
            },
        },
    ]

    // sorting
    const sorted = [
        {
            name: 'Alphabetical',
            func: (a, b) => {
                return a.title.localeCompare(b.title)
            },
        },
        {
            name: 'Cost (low to high)',
            func: (a, b) => {
                return a.cost - b.cost
            },
        },
        {
            name: 'Price (high to low)',
            func: (a, b) => {
                return b.cost - a.cost
            },
        },
        {
            name: 'Calories (low to high)',
            func: (a, b) => {
                return a.calories - b.calories
            },
        },
        {
            name: 'Calories (high to low)',
            func: (a, b) => {
                return b.calories - a.calories
            },
        },
    ]

    async function updateSearchQuery(searchQuery) {
        var filtered = ingredientData;
            filtered = ingredientData.filter((data) => {
                if(searchQuery.includes(" ")){
                    var temp = searchQuery.split(" ")
                    for(var i = 0; i < temp.length; i++){
                        if(data.title.toLowerCase().includes(temp[i].toLowerCase())){
                            return data;
                        }
                    }
                }else if(searchQuery == ""){
                    return data;
                } else if(data.title.toLowerCase().includes(searchQuery.toLowerCase())){
                    return data;
                }
            })
        setSeachQuery(searchQuery)
        // currRecipes(filtered)
        setSearchData(filtered.filter(filters[activeFilter].func).sort(sorted[activeSort].func))
        setCurrentData(searchData.slice(0, CardsPerPage));
    }


    async function filterBy(option) {
        setActiveFilter(option.value)
        let filtered = searchData.filter(filters[option.value].func)
        setSearchData(filtered)
        setCurrentData(filtered.slice(0,CardsPerPage))
    }

     async function sortBy(option) {
        setActiveSort(option.value)

        let filtered = searchData.sort(sorted[option.value].func)
        setSearchData(filtered)

        // setSearchData(searchData.sort(sorted[option.value].func))
        setCurrentData(searchData.slice(0,CardsPerPage))
        // console.log("currRecipesSort", currRecipes)
        // console.log("searchDataSort", searchData)
    }


    function handleChangePage(pageNumber) {
        setCurrentData(
            searchData.slice(
                pageNumber * CardsPerPage - CardsPerPage,
                pageNumber * CardsPerPage
            )
        )
    }

    var numTotalPages = Math.ceil(searchData.length / CardsPerPage)


    return(
        <div className= {divColor ? 'primary-container-active' : 'primary-container'}>
            <Navbar2 />
            <Container className= {divColor ? 'primary-container-active' : 'primary-container'} fluid="md mt-5">
                <Row className="justify-content-md-center">
                    <h1>Ingredients:</h1>
                </Row>
                <Row className="justify-content-md-center mb-3">
                    {/* <CustomPagination
                        itemsPerPage={ingredientsPerPage}
                        totalItems={data.length}
                        paginate={paginate}
                        currPage={currPage}
                    /> */}
                    <Pagination
                    count={numTotalPages}
                    onChange={(_, page) => handleChangePage(page)}
                    showFirstButton
                    showLastButton
                />
                </Row>

                <Select
                placeholder="Sort by..."
                options={sorted.map((e, i) => {
                    return { value: i, label: e.name }
                })}
                onChange={sortBy}
                />


                <Select
                placeholder="Filter by..."
                options={filters.map((e, i) => {
                    return { value: i, label: e.name }
                })}
                onChange={filterBy}
                />

                <SearchBar searchQuery={searchQuery} onChange={updateSearchQuery} />
                <IngredientGrid
                    ingredientData={currentData}
                    searchQuery={searchQuery}>
                </IngredientGrid>

                {/* <Row>
                    {currIngredients.map(ingredient =>(
                        <Col>
                            <Ingredient ingredientData={ingredient} />
                        </Col>
                    ))}
                </Row> */}
            </Container>
        </div>
    );
}

export default Ingredients;
