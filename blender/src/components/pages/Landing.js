import React from 'react';
import "../../App.css";
import Navbar from '../Navbar.js'
import Homelayout from '../HomeLayout';

function landing() {
    return(
        <>
            <Navbar/>
            <Homelayout />
        </>
    )
}
export default landing;
