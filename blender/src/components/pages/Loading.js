import React from 'react';

import { Spinner } from 'react-bootstrap';
import Navbar2 from '../Navbar2.js';

const Loading = () => {

    return (
        <div>
            <Navbar2 />
            <div style={{
                display: 'flex',
                alignItem: 'center',
                justifyContent: 'center',
            }}>
                <Spinner animation="border" role="status">
                    <span className="sr-only">Loading...</span>
                </Spinner>
            </div>
        </div>
    );
};

export default Loading;
