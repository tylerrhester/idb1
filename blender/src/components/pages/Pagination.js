import React, {useEffect, useState} from 'react';

import { Pagination } from 'react-bootstrap';

// For basic pagination tutorial
// https://www.youtube.com/watch?v=IYCa1F-OWmk

const CustomPagination = ({ itemsPerPage, totalItems, paginate, currPage }) => {

    const pageNumbers = [];
    const numPages = Math.ceil(totalItems / itemsPerPage);

    for (let i = 1; i <= numPages; i++) {
        pageNumbers.push(i);
    }

    // Don't need the ellipsis portion of the pagination if the number of items is small enough
    if (numPages < 10) {
        return (
            <nav>
                <Pagination>
                    <Pagination.First onClick={()=>paginate(1)}/>
                    <Pagination.Prev onClick={()=> (
                        currPage > 1 ? paginate(currPage-1) : console.log("Can't go back")
                    )}/>

                    {pageNumbers.map(num => (
                        <Pagination.Item
                            active={currPage === num}
                            onClick={() =>
                                paginate(num)
                            }
                        >
                            {num}
                        </Pagination.Item>
                    ))}

                    <Pagination.Next onClick={()=> (
                        currPage < numPages ? paginate(currPage+1) : console.log("Can't go forward")
                    )}/>
                    <Pagination.Last onClick={()=>paginate(numPages)}/>
                </Pagination>
            </nav>
        );
    }

    return (
        <nav>
            <Pagination>
                <Pagination.First onClick={()=>paginate(1)}/>
                <Pagination.Prev onClick={()=> (
                    currPage > 1 ? paginate(currPage-1) : console.log("Start")
                )}/>

                {currPage <= 3 &&
                    <React.Fragment>
                        <Pagination.Item active={currPage === 1} onClick={()=>paginate(1)}>{1}</Pagination.Item>
                        <Pagination.Item active={currPage === 2} onClick={()=>paginate(2)}>{2}</Pagination.Item>
                        <Pagination.Item active={currPage === 3} onClick={()=>paginate(3)}>{3}</Pagination.Item>
                        <Pagination.Ellipsis onClick={()=>paginate(currPage+3)}/>
                    </React.Fragment>
                }

                {currPage > 3 && currPage < numPages-2 &&
                    <React.Fragment>
                        <Pagination.Ellipsis onClick={()=>paginate(currPage-2)}/>
                        <Pagination.Item onClick={()=>paginate(currPage-1)}>{currPage-1}</Pagination.Item>
                        <Pagination.Item active onClick={()=>paginate(currPage)}>{currPage}</Pagination.Item>
                        <Pagination.Item onClick={()=>paginate(currPage+1)}>{currPage+1}</Pagination.Item>
                        <Pagination.Ellipsis onClick={()=>paginate(currPage+2)}/>
                    </React.Fragment>
                }

                {currPage >= numPages-2 &&
                    <React.Fragment>
                        <Pagination.Ellipsis onClick={()=>paginate(numPages-3)}/>
                        <Pagination.Item active={currPage === numPages-2} onClick={()=>paginate(numPages-2)}>{numPages-2}</Pagination.Item>
                        <Pagination.Item active={currPage === numPages-1} onClick={()=>paginate(numPages-1)}>{numPages-1}</Pagination.Item>
                        <Pagination.Item active={currPage === numPages} onClick={()=>paginate(numPages)}>{numPages}</Pagination.Item>
                    </React.Fragment>
                }

                <Pagination.Next onClick={()=> (
                    currPage < numPages ? paginate(currPage+1) : console.log("End")
                )}/>
                <Pagination.Last onClick={()=>paginate(numPages)}/>
            </Pagination>
        </nav>
    );
};

export default CustomPagination;
