import React, {useEffect, useState} from 'react';
import { Card, ListGroup } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles'
import './Recipes.css'
import Typography from '@material-ui/core/Typography'
import CardContent from '@material-ui/core/CardContent'
import { CardMedia } from '@material-ui/core'
import Cards from '@material-ui/core/Card'
import Highlighter from 'react-highlight-words'

const useStyles = makeStyles({
    root: {
        minWidth: 275,
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        fontSize: 14,
    },
    pos: {
        marginBottom: 12,
    },
    media: {
        height: 170,
    },
})

function returnString(recipeData) {
    if (recipeData === undefined) {
        return 'N/A'
    } else {
        return recipeData.toString()
    }
}

const Recipe = ({ recipeData, searchQuery }) => {
    console.log(searchQuery)

    const [divColor, setBagColor] = useState(false);
    const changeBackground = () => {
        if(window.scrollY >= 135){
            setBagColor(true);
        }else{
            setBagColor(false);
        }
    }
    // var termArray = searchQuery.split(" ")

    window.addEventListener('scroll', changeBackground);
    const classes = useStyles()

    return (
         <a href={'/recipe/' + recipeData.id}>
            <Cards className={classes.root} variant="outlined">
                <CardContent>
                    <CardMedia
                        className={classes.media}
                        image={recipeData.image}
                        title="image"
                    />
                    <Typography
                        className={classes.pos}
                        variant="h6"
                        component="h2"
                        align="center"
                    >
                        <Highlighter
                            searchWords={[searchQuery]}
                            textToHighlight={recipeData.title}
                        />
                    </Typography>
                    <Typography className={classes.pos}>
                        Price Per Serving: ${recipeData.pricePerServing}
                        <Highlighter
                            searchWords={[searchQuery]}
                            textToHighlight={recipeData.pricePerServing}/>
                    </Typography>
                    <Typography className={classes.pos}>
                        Number of Servings:{recipeData.numServings}
                        <Highlighter
                            searchWords={[searchQuery]}
                            textToHighlight={recipeData.numServings}/>
                    </Typography>
                    <Typography className={classes.pos}>
                        Time to Cook:{recipeData.readyMin}
                        <Highlighter
                            searchWords={[searchQuery]}
                            textToHighlight={recipeData.readyMin}/>
                    </Typography>
                </CardContent>
            </Cards>
            </a>

    // <div>
    //     <Card style={{ width: '18rem' }}>
    //         <Card.Header className= {divColor ? 'primary-container-active' : 'primary-container'}>
    //             <Link className= {divColor ? 'primary-container-active1' : 'primary-container1'} to={{pathname:`/recipe/${recipeData.id}`, query: `${recipeData.id}`}}>
    //                 {recipeData.title}
    //             </Link>
    //         </Card.Header>
    //         <Card.Img src={recipeData.image} variant="top" />
    //         <ListGroup className= {divColor ? 'primary-container-active' : 'primary-container'}>
    //             <ListGroup.Item className= {divColor ? 'primary-container-active' : 'primary-container'}>Price Per Serving: ${recipeData.pricePerServing}</ListGroup.Item>
    //             <ListGroup.Item className= {divColor ? 'primary-container-active' : 'primary-container'}>Number of Servings: {recipeData.numServings}</ListGroup.Item>
    //             <ListGroup.Item className= {divColor ? 'primary-container-active' : 'primary-container'}>Time to Cook: {recipeData.readyMin} minutes</ListGroup.Item>
    //         </ListGroup>
    //     </Card>
    //     </div>
    );
};

export default Recipe;
