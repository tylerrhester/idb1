import React, { useEffect, useState } from 'react';

import Navbar2 from '../Navbar2.js';
import Recipe from './RecipeCard.js';
import Ingredient from './IngredientCard.js';
import Wine from './WineCard.js';
import CustomPagination from './Pagination.js';

import { Card, Col, Container, Row } from 'react-bootstrap';
import { useParams } from 'react-router-dom';

import axios from 'axios';

const RecipeDetails = () => {

    // filtering

    const { id } = useParams();
    const [data, setData] = useState([]);
    const [ings, setIngs] = useState([]);

    const [loading, setLoading] = useState(false);
    const [currPage, setCurrentPage] = useState(1);
    const [ingsPerPage] = useState(3);

    const lastIng = currPage * ingsPerPage;
    const firstIng = lastIng - ingsPerPage;
    const currIngs = ings.slice(firstIng, lastIng);
    const paginate = (pageNumber) => setCurrentPage(pageNumber);


    useEffect(() => {
        const getRecipe = async () => {
            setLoading(true);
            const singleRecipe = await axios.get("http://api-dot-friendly-autumn-317817.uc.r.appspot.com/api/recipe/"+id);
            setData(singleRecipe.data);
            setIngs(singleRecipe.data.recipeIngredients);
            setLoading(false);
        };

        getRecipe();
    }, [id]);

    return (
        <div>
            <Navbar2 />
            <Container fluid="md mt-5">
                <Row className="justify-content-md-center">
                    <Col className="d-flex justify-content-center">
                        <Recipe recipeData={data}/>
                    </Col>
                    <Col>
                        <Card>
                            <Card.Header>
                                Summary:
                            </Card.Header>
                            <Card.Body>
                                <div dangerouslySetInnerHTML={{ __html: data.summary }} />
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>

                <Row className="mt-5 mb-5">
                    <Card>
                        <Card.Header>
                            Cooking Instructions:
                        </Card.Header>
                        <Card.Body>
                            <div dangerouslySetInnerHTML={{ __html: data.instructions }} />
                        </Card.Body>
                    </Card>
                </Row>

                <h3 className="text-center">Ingredients Used:</h3>
                <Row className="justify-content-md-center mb-3">
                    <CustomPagination
                        itemsPerPage={ingsPerPage}
                        totalItems={ings.length}
                        paginate={paginate}
                        className="justify-content-center"
                    />
                </Row>
                <Row>
                    {currIngs.map(ingredient => (
                        <Col className="justify-content-md-center">
                            <Ingredient ingredientData={ingredient} />
                        </Col>
                    ))}
                </Row>

                <h3 className="text-center mt-5">Wine Pairings:</h3>
                <Row className="mb-5">
                    {data.recipeWines?.map(wine => (
                        <Col>
                            <Wine wineData={wine} />
                        </Col>
                    ))}
                </Row>
            </Container>
        </div>
    );
};

export default RecipeDetails;
