import React from 'react'
import RecipeCard from './RecipeCard'
import { Grid } from '@material-ui/core'


function RecipeGrid({ recipeData, searchQuery }) {
 
    return (
        <Grid container spacing={3} justify="center">
            {recipeData.map((col) => {
                return (
                    <Grid item key={col.id} xs={4}>
                        <RecipeCard recipeData={col} searchQuery={searchQuery} />
                    </Grid>
                )
            })}
        </Grid>
    )
}
export default RecipeGrid
