import React, {useEffect, useState} from 'react';
import { Container, Col, Row } from 'react-bootstrap';
import axios from 'axios';

import Navbar2 from '../Navbar2.js';
import Recipe from './RecipeCard.js';
import CustomPagination from './Pagination.js';
import { Pagination } from '@material-ui/lab'
import Loading from './Loading.js';
import './Recipes.css'
import "../../App.css";
import Select from 'react-select'
import SearchBar from '../SearchBar.js'
import RecipeGrid from './RecipeGrid'

const Recipes = () => {

    const CardsPerPage = 9

    // background change on scrol. Smooth transition.
    const [divColor, setBagColor] = useState(false);
    const changeBackground = () => {
        if(window.scrollY >= 135){
            setBagColor(true);
        }else{
            setBagColor(false);
        }
    }
    window.addEventListener('scroll', changeBackground);

    const [data, setData] = useState([]);
    const [loading, setLoading] = useState(false);
    const [currPage, setCurrentPage] = useState(1);
    const [recipesPerPage] = useState(6);


    // filter, sort, search, var options.
    var [recipesData, setRecipesData] = useState([])
    var [currentData, setCurrentData] = useState([])
    var [activeFilter, setActiveFilter] = useState(0);
    var [activeSort, setActiveSort] = useState(0);
    var [searchData, setSearchData] = useState([])
    var [searchQuery, setSeachQuery] = useState('')


    const lastRecipe = currPage * recipesPerPage;
    const firstRecipe = lastRecipe - recipesPerPage;
    const currRecipes = data.slice(firstRecipe, lastRecipe);
    const paginate = (pageNumber) => setCurrentPage(pageNumber);

    useEffect(() => {
        const getRecipes = async () => {
            setLoading(true);
            const allRecipes = await axios.get("http://api-dot-friendly-autumn-317817.uc.r.appspot.com/api/recipes");

            // tyler
            setData(allRecipes.data.Recipes);

            // me
            setRecipesData(allRecipes.data.Recipes);
            setSearchData(allRecipes.data.Recipes);
            setCurrentData(allRecipes.data.Recipes.slice(0, CardsPerPage));

            setLoading(false);
        };

        getRecipes();
    }, []);

    if (loading) {
        return (
            <Loading />
        );
    }


    // filterting.
    const filters = [
        {
            name: 'None',
            func: (a) => {
                return a.readyMin > 1
            },
        },
        {
            name: 'Time To Cook <= 45 mintues',
            func: (a) => {
                return a.readyMin <= 45
            },
        },
        {
            name: 'Time To Cook > 45 mintues',
            func: (a) => {
                return a.readyMin > 45
            },
        },
        {
            name: 'Price Per Serving <= $100',
            func: (a) => {
                return a.pricePerServing <= 100
            },
        },
        {
            name: ' $100 <= Price Per Serving <= 200',
            func: (a) => {
                return a.pricePerServing >= 100 && a.pricePerServing <= 200
            },
        },
        {
            name: 'Price Per Serving >= 200',
            func: (a) => {
                return a.pricePerServing >= 200
            },
        },
        {
            name: 'Number of Servings for 1',
            func: (a) => {
                return a.numServings === 1
            },
        },
        {
            name: 'Number of Servings for 2',
            func: (a) => {
                return a.numServings === 2
            },
        },
        {
            name: 'Number of Serving for 3',
            func: (a) => {
                return a.numServings === 3
            },
        },
        {
            name: 'Number of Serving for 4',
            func: (a) => {
                return a.numServings === 4
            }
        },
        {
            name: 'Number of Serving: Family style',
            func: (a) => {
                return a.numServings > 4
            }
        }
    ]

    // sorting
    const sorted = [
        {
            name: 'Alphabetical',
            func: (a, b) => {
                return a.title.localeCompare(b.title)
            },
        },
        {
            name: 'Price (low to high)',
            func: (a, b) => {
                return a.pricePerServing - b.pricePerServing
            },
        },
        {
            name: 'Price (high to low)',
            func: (a, b) => {
                return b.pricePerServing - a.pricePerServing
            },
        },
        {
            name: 'Servings (low to high)',
            func: (a, b) => {
                return a.numServings - b.numServings
            },
        },
        {
            name: 'Servings (high to low)',
            func: (a, b) => {
                return b.numServings - a.numServings
            },
        },
    ]


    async function updateSearchQuery(searchQuery) {
        var filtered = recipesData;

            filtered = recipesData.filter((data) => {
                if(searchQuery.includes(" ")){
                    var temp = searchQuery.split(" ")
                    for(var i = 0; i < temp.length; i++){
                        if(data.title.toLowerCase().includes(temp[i].toLowerCase())){
                            return data;
                        }
                    }
                }else if(searchQuery === ""){
                    return data;
                } else if(data.title.toLowerCase().includes(searchQuery.toLowerCase())){
                    return data;
                }
            }
        )

        setSeachQuery(searchQuery)
        // currRecipes(filtered)
        setSearchData(filtered.filter(filters[activeFilter].func).sort(sorted[activeSort].func))
        setCurrentData(searchData.slice(0, CardsPerPage));
    }

    async function filterBy(option) {
        setActiveFilter(option.value)
        let filtered = searchData.filter(filters[option.value].func)
        setSearchData(filtered)
        setCurrentData(filtered.slice(0,CardsPerPage))
    }

     async function sortBy(option) {
        setActiveSort(option.value)

        let filtered = searchData.sort(sorted[option.value].func)
        setSearchData(filtered)

        // setSearchData(searchData.sort(sorted[option.value].func))
        setCurrentData(searchData.slice(0,CardsPerPage))
        // console.log("currRecipesSort", currRecipes)
        // console.log("searchDataSort", searchData)
    }


    function handleChangePage(pageNumber) {
        setCurrentData(
            searchData.slice(
                pageNumber * CardsPerPage - CardsPerPage,
                pageNumber * CardsPerPage
            )
        )
    }

    var numTotalPages = Math.ceil(searchData.length / CardsPerPage)

    return(
        <div className= {divColor ? 'primary-container-active' : 'primary-container'}>
            <Navbar2/>
            <Container className= {divColor ? 'primary-container-active' : 'primary-container'} fluid="md mt-5">
                <Row className="justify-content-md-center">
                    <h1>Recipes:</h1>
                </Row>

                <Row className= "justify-content-md-center mb-3" >
                    {/* <CustomPagination
                        itemsPerPage={recipesPerPage}
                        totalItems={currentData.length}
                        paginate={paginate}
                        currPage={currPage}
                    /> */}
                    <Pagination
                    count={numTotalPages}
                    onChange={(_, page) => handleChangePage(page)}
                    showFirstButton
                    showLastButton
                />

                </Row>

                <Select
                placeholder="Sort by..."
                options={sorted.map((e, i) => {
                    return { value: i, label: e.name }
                })}
                onChange={sortBy}
                />


                <Select
                placeholder="Filter by..."
                options={filters.map((e, i) => {
                    return { value: i, label: e.name }
                })}
                onChange={filterBy}
                />

                <SearchBar searchQuery={searchQuery} onChange={updateSearchQuery} />
                <RecipeGrid
                    recipeData={currentData}
                    searchQuery={searchQuery}>
                </RecipeGrid>

                {/* <Row>
                    {currentData.map(recipe =>(
                        <Col>
                            <Recipe
                                recipeData={recipe}
                                searchQuery={searchQuery}
                                />
                        </Col>
                    ))}
                </Row> */}

            </Container>
        </div>
    );
}

export default Recipes;
