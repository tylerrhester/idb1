import React, {useEffect, useState} from 'react';
import Navbar2 from '../Navbar2.js';
import { Container, Col, Row } from 'react-bootstrap';
import Recipe from './RecipeCard.js';
import Ingredient from './IngredientCard.js';
import Wine from './WineCard.js';
import CustomPagination from './Pagination.js';
import axios from 'axios'
import { Button } from '../CustomButton';

var Search = ({searchTerm}) => {

    var [searchTerm, setSearchTerm] = useState("" || searchTerm);

    const BarStyling = {width:"23rem",background:"#F2F1F9", border:"none", padding:"0.7rem"};

    var [recipesData, setRecipesData] = useState([])
    var [ingredientsData, setIngredientsData] = useState([])
    var [winesData, setWinesData] = useState([])

    useEffect(() => {
        const getData = async () => {
            const allRecipes = await axios.get("http://friendly-autumn-317817.uc.r.appspot.com/api/recipes");
            const allIngredients = await axios.get("http://friendly-autumn-317817.uc.r.appspot.com/api/ingredients");
            const allWines = await axios.get("http://friendly-autumn-317817.uc.r.appspot.com/api/wines");
            setRecipesData(allRecipes.data.Recipes);
            setIngredientsData(allIngredients.data.Ingredients);
            setWinesData(allWines.data.Wines);
        };

        getData();
    }, []);

    recipesData = recipesData.filter((recipe) => {
        if (searchTerm.includes(" ")) {
            var termArray = searchTerm.split(" ")
            for (var i = 0; i < termArray.length; i++) {
                if (recipe.title.toLowerCase().includes(termArray[i].toLowerCase())) {
                    return recipe;
                }
            }
        } else if (searchTerm == "") {
            return recipe;
        } else if (recipe.title.toLowerCase().includes(searchTerm.toLowerCase())) {
            return recipe;
        }
    })

    ingredientsData = ingredientsData.filter((val) => {
        if (searchTerm.includes(" ")) {
            var termArray = searchTerm.split(" ")
            for (var i = 0; i < termArray.length; i++) {
                if (val.title.toLowerCase().includes(termArray[i].toLowerCase())) {
                    return val;
                }
            }
        } else if (searchTerm == "") {
            return val;
        } else if (val.title.toLowerCase().includes(searchTerm.toLowerCase())) {
            return val;
        }
    })

    winesData = winesData.filter((val) => {
        if (searchTerm.includes(" ")) {
            var termArray = searchTerm.split(" ")
            for (var i = 0; i < termArray.length; i++) {
                if (val.title.toLowerCase().includes(termArray[i].toLowerCase())) {
                    return val;
                }
            }
        } else if (searchTerm == "") {
            return val;
        } else if (val.title.toLowerCase().includes(searchTerm.toLowerCase())) {
            return val;
        }
    })


    const [itemsPerPage] = useState(3);

    const [currRPage, setCurrentRPage] = useState(1);
    const paginateR = (pageNumber) => setCurrentRPage(pageNumber);

    const [currIPage, setCurrentIPage] = useState(1);
    const paginateI = (pageNumber) => setCurrentIPage(pageNumber);

    const [currWPage, setCurrentWPage] = useState(1);
    const paginateW = (pageNumber) => setCurrentWPage(pageNumber);

    const lastRecipe = currRPage * itemsPerPage;
    const firstRecipe = lastRecipe - itemsPerPage;
    const currRecipes = recipesData.slice(firstRecipe, lastRecipe);

    const lastIng = currIPage * itemsPerPage;
    const firstIng = lastIng - itemsPerPage;
    const currIngs = ingredientsData.slice(firstIng, lastIng);

    const lastWine = currWPage * itemsPerPage;
    const firstWine = lastWine - itemsPerPage;
    const currWines = winesData.slice(firstWine, lastWine);

    return (
        <div>
            <Navbar2/>
            <Container fluid="md mt-5">
                <Row className="justify-content-md-center">
                <form method="get">
                    <input
                        type="text"
                        id="header-search"
                        name="s"
                        style={BarStyling}
                        placeholder={"search recipes, ingredients, or wines"}
                        value={searchTerm}
                        onChange={(e) => setSearchTerm(e.target.value)}

                    />
                </form>
                </Row>
                <br></br>
                <Row className="justify-content-md-center">
                    <h3>Showing results for "{searchTerm}"</h3>
                </Row>
                <br></br>
                <Row className="justify-content-md-center">
                    <h3>Recipes:</h3>
                </Row>
                <Row className= "justify-content-md-center mb-3" >
                    <CustomPagination
                        itemsPerPage={itemsPerPage}
                        totalItems={recipesData.length}
                        paginate={paginateR}
                        currPage={currRPage}
                    />
                </Row>
                <Row>

                    {currRecipes.map((recipe) => {
                        return (
                            <Col>
                                <Recipe
                                    recipeData={recipe}
                                    searchQuery={searchTerm}
                                />
                            </Col>
                        );
                    })}
                </Row>
                <br></br>
                <Row className="justify-content-md-center">
                    <h3>Ingredients:</h3>
                </Row>
                <Row className= "justify-content-md-center mb-3" >
                    <CustomPagination
                        itemsPerPage={itemsPerPage}
                        totalItems={ingredientsData.length}
                        paginate={paginateI}
                        currPage={currIPage}
                    />
                </Row>
                <Row>

                    {currIngs.map((ing) => {
                        return (
                            <Col>
                                <Ingredient
                                    ingredientData={ing}
                                    searchQuery={searchTerm}
                                />
                            </Col>
                        );
                    })}
                </Row>
                <br></br>
                <Row className="justify-content-md-center">
                    <h3>Wines:</h3>
                </Row>
                <Row className= "justify-content-md-center mb-3" >
                    <CustomPagination
                        itemsPerPage={itemsPerPage}
                        totalItems={winesData.length}
                        paginate={paginateW}
                        currPage={currWPage}
                    />
                </Row>
                <Row>
                    {currWines.map((wine) => {
                        return (
                            <Col>
                                <Wine
                                    wineData={wine}
                                    searchQuery={searchTerm}
                                />
                            </Col>
                        );
                    })}
                </Row>

            </Container>
        </div>
    );
}

export default Search;
