import React from 'react';
import { Card, ListGroup } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Typography from '@material-ui/core/Typography'
import CardContent from '@material-ui/core/CardContent'
import { CardMedia } from '@material-ui/core'
import Cards from '@material-ui/core/Card'
import Highlighter from 'react-highlight-words'
import { makeStyles } from '@material-ui/core/styles'


function returnString(wineData) {
    if (wineData === undefined) {
        return 'N/A'
    } else {
        return wineData.toString()
    }
}

const useStyles = makeStyles({
    root: {
        minWidth: 275,
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        fontSize: 14,
    },
    pos: {
        marginBottom: 12,
    },
    media: {
        height: 170,
    },
})

const Wine = ({ wineData, searchQuery }) => {

    // var termArray = searchQuery.split(" ")
    const classes = useStyles()
    return (

        <a href={'/wine/' + wineData.id}>
        <Cards className={classes.root} variant="outlined">
            <CardContent>
                <CardMedia
                    className={classes.media}
                    image={wineData.sourceIMG}
                    title="image"
                />
                <Typography
                    className={classes.pos}
                    variant="h6"
                    component="h2"
                    align="center"
                >
                    <Highlighter
                        searchWords={[searchQuery]}
                        textToHighlight={wineData.title}
                    />
                </Typography>
                <Typography className={classes.pos}>
                    price: {" "}
                    <Highlighter
                        searchWords={[searchQuery]}
                        textToHighlight={wineData.price}/>
                </Typography>
                <Typography className={classes.pos}>
                    Rating: {wineData.rating}
                    <Highlighter
                        searchWords={[searchQuery]}
                        textToHighlight={wineData.rating}/>
                </Typography>
            </CardContent>
        </Cards>

        </a>

        // <Card style={{ width: '18rem' }}>
        //     <Card.Header>
        //         <Link to={{pathname:`/wine/${wineData.id}`, query: `${wineData.id}`}}>
        //             {wineData.title}
        //         </Link>
        //     </Card.Header>
        //     <Card.Img src={wineData.sourceIMG} variant="top" />
        //     <ListGroup className="list-group-flush">
        //         <ListGroup.Item>Price: {wineData.price}</ListGroup.Item>
        //         <ListGroup.Item>Rating: {wineData.rating}</ListGroup.Item>
        //         <ListGroup.Item>Num. of Ratings: {wineData.ratingCount}</ListGroup.Item>
        //     </ListGroup>
        // </Card>
    );
};

export default Wine;
