import React, { useEffect, useState } from 'react';

import Navbar2 from '../Navbar2.js'
import Wine from './WineCard.js'
import Recipe from './RecipeCard.js'
import Ingredient from './IngredientCard.js'
import CustomPagination from './Pagination.js'

import { Container, Row, Col, Card} from 'react-bootstrap';
import { useParams } from 'react-router-dom';

import axios from 'axios';
import "../../App.css";

const WineDetails = () => {

    const { id } = useParams();

    const [data, setData] = useState([]);
    const [wineIngs, setWineIngs] = useState([]);

    const [currPage, setCurrentPage] = useState(1);
    const [ingsPerPage] = useState(3);

    const lastIng = currPage * ingsPerPage;
    const firstIng = lastIng - ingsPerPage;
    const currIngs = wineIngs.slice(firstIng, lastIng);
    const paginate = (pageNumber) => setCurrentPage(pageNumber);

    useEffect(() => {
        const getRecipe = async () => {
            const singleWine = await axios.get("http://api-dot-friendly-autumn-317817.uc.r.appspot.com/api/wine/"+id);
            setData(singleWine.data);
            setWineIngs(singleWine.data.wineIngs);
        };

        getRecipe();
    }, [id]);

    return (
        <div>
            <Navbar2 />
            <Container fluid="md mt-5">
                <Row className="mb-5">
                    <Col className="d-flex justify-content-center">
                        <Wine wineData={data}/>
                    </Col>
                    <Col>
                        <Card>
                            <Card.Header>
                                <Card.Title>
                                    Description:
                                </Card.Title>
                            </Card.Header>
                            <Card.Body>
                                {data.description}
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>

                <h3 className="text-center">Recipe Pairings:</h3>
                <Row className="mb-5">
                    {data.wineRecipes?.map(recipe => (
                        <Col>
                            <Recipe recipeData={recipe} />
                        </Col>
                    ))}
                </Row>

                <h3 className="text-center">Ingredient Pairings:</h3>
                <Row className="justify-content-md-center mb-3">
                    <CustomPagination
                        itemsPerPage={ingsPerPage}
                        totalItems={wineIngs.length}
                        paginate={paginate}
                        currPage={currPage}
                    />
                </Row>
                <Row>
                    {currIngs.map(ing => (
                        <Col>
                            <Ingredient ingredientData={ing} />
                        </Col>
                    ))}
                </Row>

            </Container>
        </div>
    );
};

export default WineDetails;
