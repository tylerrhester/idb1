import React from 'react'
import WineCard from './WineCard'
import { Grid } from '@material-ui/core'


function WineGrid({ wineData, searchQuery }) {
    return (
        <Grid container spacing={3} justify="center">
            {wineData.map((col) => {
                return (
                    <Grid item key={col.id} xs={4}>
                        <WineCard wineData={col} searchQuery={searchQuery} />
                    </Grid>
                )
            })}
        </Grid>
    )
}
export default WineGrid
