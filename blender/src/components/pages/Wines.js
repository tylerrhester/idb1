import React, {useEffect, useState} from 'react';
import { Row, Col, Container } from 'react-bootstrap';
import "../../App.css";
import Wine from './WineCard.js'
import Navbar2 from '../Navbar2.js'
import CustomPagination from './Pagination.js';
import Loading from './Loading.js';
import axios from 'axios';
import Select from 'react-select'
import SearchBar from '../SearchBar.js'
import WineGrid from './WineGrid'
import { Pagination } from '@material-ui/lab'

const CardsPerPage = 9

function Wines() {



    var [wineData, setWineData] = useState([])
    var [currentData, setCurrentData] = useState([])
    var [activeFilter, setActiveFilter] = useState(0);
    var [activeSort, setActiveSort] = useState(0);
    var [searchData, setSearchData] = useState([])
    var [searchQuery, setSeachQuery] = useState('')

    const [data, setData] = useState([]);
    const [loading, setLoading] = useState(false);
    const [currPage, setCurrentPage] = useState(1);
    const [winesPerPage] = useState(6);

    const lastWine = currPage * winesPerPage;
    const firstWine = lastWine - winesPerPage;
    const currWines = data.slice(firstWine, lastWine);
    const paginate = (pageNumber) => setCurrentPage(pageNumber);


    useEffect(() => {
        const getWines = async () => {
            setLoading(true);
            const allWines = await axios.get("http://api-dot-friendly-autumn-317817.uc.r.appspot.com/api/wines");
            setData(allWines.data.Wines);
            setWineData(allWines.data.Wines);
            setSearchData(allWines.data.Wines);
            setCurrentData(allWines.data.Wines.slice(0, CardsPerPage));
            console.log(allWines.data.Wines[0].price.toString())
            setLoading(false);
        };
        getWines();
    }, []);

    if (loading) {
        return (
            <Loading />
        );
    }

    const filters = [
        {
            name: 'None',
            func: (a) => {
                return true
            },
        },
        {
            name: 'price < 20',
            func: (a) => {
                return a.price < 20
            },
        },
        {
            name: ' 20 <= price < 50',
            func: (a) => {
                return a.price >= 20 && a.price < 50
            },
        },
        {
            name: '50 <= price <= 100',
            func: (a) => {
                return a.price >= 50 && a.price <= 100
            },
        },
        {
            name: ' 0 <= rating <= 0.1',
            func: (a) => {
                return a.rating >= 0 && a.rating <= 0.1
            },
        },
        {
            name: ' 0.1 <= rating <= 0.2',
            func: (a) => {
                return a.rating >= 1 && a.rating <= 0.2
            },
        },
        {
            name: ' 0.2 <= rating <= 3',
            func: (a) => {
                return a.rating >= 0.2 && a.rating <= 0.3
            },
        },
        {
            name: ' 0.3 <= rating <= 0.4',
            func: (a) => {
                return a.rating >= 0.3 && a.rating <= 0.4
            },
        },
        {
            name: ' rating >= 0.7',
            func: (a) => {
                return a.rating >= 0.7
            },
        },
        {
            name: 'Number of ratings < 10',
            func: (a) => {
                return a.ratingCount < 10
            },
        },
        {
            name: 'Number of ratings >= 10',
            func: (a) => {
                return a.ratingCount >= 10
            },
        },
    ]

    // sorting
    const sorted = [
        {
            name: 'Alphabetical',
            func: (a, b) => {
                return a.title.localeCompare(b.title)
            },
        },
        {
            name: 'Price (low to high)',
            func: (a, b) => {
                return a.price - b.price
            },
        },
        {
            name: 'Price (high to low)',
            func: (a, b) => {
                return b.price - a.price
            },
        },
        {
            name: 'Rating (low to high)',
            func: (a, b) => {
                return a.rating - b.rating
            },
        },
        {
            name: 'Rating (high to low)',
            func: (a, b) => {
                return b.rating - a.rating
            },
        },
        {
            name: 'Number of Ratings (low to high)',
            func: (a, b) => {
                return a.ratingCount - b.ratingCount
            },
        },
        {
            name: 'Number of Ratings (high to low)',
            func: (a, b) => {
                return b.ratingCount - a.ratingCount
            },
        },
    ]

    async function updateSearchQuery(searchQuery) {
        var filtered = wineData;

            filtered = wineData.filter((data) => {
                if(searchQuery.includes(" ")){
                    var temp = searchQuery.split(" ")
                    for(var i = 0; i < temp.length; i++){
                        if(data.title.toLowerCase().includes(temp[i].toLowerCase())){
                            return data;
                        }
                    }
                }else if(searchQuery == ""){
                    return data;
                } else if(data.title.toLowerCase().includes(searchQuery.toLowerCase())){
                    return data;
                }
            })
        setSeachQuery(searchQuery)
        // currRecipes(filtered)
        setSearchData(filtered.filter(filters[activeFilter].func).sort(sorted[activeSort].func))
        setCurrentData(searchData.slice(0, CardsPerPage));
    }


    async function filterBy(option) {
        setActiveFilter(option.value)
        let filtered = searchData.filter(filters[option.value].func)
        setSearchData(filtered)
        setCurrentData(filtered.slice(0,CardsPerPage))
    }

     async function sortBy(option) {
        setActiveSort(option.value)

        let filtered = searchData.sort(sorted[option.value].func)
        setSearchData(filtered)

        // setSearchData(searchData.sort(sorted[option.value].func))
        setCurrentData(searchData.slice(0,CardsPerPage))
        // console.log("currRecipesSort", currRecipes)
        // console.log("searchDataSort", searchData)
    }


    function handleChangePage(pageNumber) {
        setCurrentData(
            searchData.slice(
                pageNumber * CardsPerPage - CardsPerPage,
                pageNumber * CardsPerPage
            )
        )
    }

    var numTotalPages = Math.ceil(searchData.length / CardsPerPage)

    return(
        <div>
            <Navbar2 />
            <Container fluid="md mt-5">
                <Row className="justify-content-md-center">
                    <h1>Wines:</h1>
                </Row>
                <Row className="justify-content-md-center mb-3">
                    {/* <CustomPagination
                        itemsPerPage={winesPerPage}
                        totalItems={data.length}
                        paginate={paginate}
                        currPage={currPage}
                    /> */}
                             <Pagination
                    count={numTotalPages}
                    onChange={(_, page) => handleChangePage(page)}
                    showFirstButton
                    showLastButton
                />
                </Row>
                <Select
                placeholder="Sort by..."
                options={sorted.map((e, i) => {
                    return { value: i, label: e.name }
                })}
                onChange={sortBy}
                />


                <Select
                placeholder="Filter by..."
                options={filters.map((e, i) => {
                    return { value: i, label: e.name }
                })}
                onChange={filterBy}
                />

                <SearchBar searchQuery={searchQuery} onChange={updateSearchQuery} />
                <WineGrid
                    wineData={currentData}
                    searchQuery={searchQuery}>
                </WineGrid>
                {/* <Row>
                    {currWines.map(wine =>(
                        <Col>
                            <Wine wineData={wine} />
                        </Col>
                    ))}
                </Row> */}
            </Container>
        </div>
    );
}

export default Wines;
