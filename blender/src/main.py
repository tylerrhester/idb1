import json
import os
import sys
import subprocess
from setupDb import app, db, create_ingredient, create_recipes, create_wine

os.chdir('./flask-server')

from flask import Flask, render_template, request
app = Flask(__name__)

@app.route('/')
def index():
    """Home page"""
    return render_template('index.html', flask_token="Hello world")
"""flask_token doesn't do anything yet, but may be useful later on"""

def convert(data):
    if (hasattr(data, '__iter__')):
        result = []
        for x in data:
            curratedData = x.__dict__
            if '_sa_instance_state' in curratedData.keys():
                curratedData.pop('_sa_instance_state')
            result.append(curratedData)
        return result
    else:
        curratedData = data.__dict__
        if '_sa_instance_state' in curratedData.keys():
            curratedData.pop('_sa_instance_state')
        return curratedData

@app.route('/api/runtests')
def runTests():
    out = subprocess.check_output([sys.executable, "tests.py"])
    return {"text": out.decode()}


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port = '80')
